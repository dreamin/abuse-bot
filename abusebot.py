import re
import random
import yaml
import discord
from discord.ext import commands

KEYWORDS = 'keywords.yml'
CONFIG = 'config.yml'
MAPS = 'maps.yml'

with open(CONFIG, 'r') as f:
    config = yaml.load(f)
    command_prefix = config['command-prefix']
    description = config['description']
    token = config['token']
botty = commands.Bot(command_prefix=command_prefix, description=description)
botty.config = config

def load_reactions():
    """Load the reactions file"""
    with open(KEYWORDS, 'r') as words:
        reactions = yaml.load(words)
        filters = {}
        for needle in reactions:
            filters[needle] = (reactions[needle]['reply'], reactions[needle]['user'], reactions[needle]['description'])
        return filters

def load_maps():
    """Load game specific maps"""
    maps = {}
    with open(MAPS, 'r') as words:
        maps = yaml.load(words)
    return maps

@botty.event
async def on_ready():
    """Called once the client is logged in and done receiving data from discord"""
    print('Logged in as')
    print(botty.user.name)
    print(botty.user.id)
    print('______')

    botty.reactions = load_reactions()
    botty.maps = load_maps()

@botty.event
async def on_message_delete(message):
    """Snitch on people who delete messages"""
    if not message.author.bot:
        # only snitch some of the time, otherwise botty ends up annoying as fuck
        if not message.author.bot:
            snitch = random.randint(1,100)
            if snitch < botty.config['snitch-propability']:
                nick = message.author.nick if message.author.nick else message.author
                await botty.send_message(
                    message.channel, 'Look guys, {} has something to hide!'.format(nick)
                )

@botty.event
async def on_message_edit(before, after):
    """Snitch on people who edit their messages?"""
    if not before.author.bot:
        # only snitch some of the time, otherwise botty ends up annoying as fuck
        snitch = random.randint(1,100)
        if snitch < botty.config['snitch-propability']:
            nick = before.author.nick if before.author.nick else before.author
            await botty.send_message(
                before.channel, 'I saw your edit, {}!'.format(nick)
            )

@botty.event
async def on_message(message):
    """Handling keyword triggers in messages"""
    # don't process abuse-bot messages
    if not message.author.bot:
        # only process reactions if message isn't a bot command
        if not message.content.startswith(command_prefix):
            for needle in botty.reactions:
                rneedle = re.escape(needle)
                if re.match(
                        re.compile(r'.*\b' +  rneedle + r'\b.*', re.IGNORECASE), message.content
                    ):
                    reply, user, description = botty.reactions[needle]
                    if user:
                        await botty.send_message(message.author, reply)
                    else:
                        await botty.send_message(message.channel, reply)
    # ensure regular commands
    await botty.process_commands(message)

@botty.command()
async def woop():
    """Basic monkey command"""
    await botty.say('Woop woop!')

@botty.command(pass_context=True)
async def maps(ctx):
    """returns map page for the game associated to the channel (if appropriate)"""
    channel = ctx.message.channel.name
    if channel in botty.maps:
        await botty.say(botty.maps[channel])

@botty.command()
async def reload_reactions():
    """Reloads the keyword/reactions configuration file"""
    botty.reactions = load_reactions()
    await botty.say('Reactions reloaded!')

@botty.command()
async def reload_config():
    """Reload configuration"""
    with open(CONFIG, 'r') as f:
        botty.config = yaml.load(f)
    await botty.say('Configuration reloaded')

@botty.command()
async def reload_maps():
    """Reload map URIs"""
    botty.maps = load_maps()
    await botty.say("Map URIs reloaded!")

botty.run(token)
