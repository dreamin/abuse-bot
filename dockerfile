from python:3

WORKDIR /usr/src/abuse-bot

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .
CMD [ "python", "./abusebot.py" ]